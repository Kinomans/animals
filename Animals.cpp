﻿#include <iostream>
using namespace std;
class Animal
{
public:
	virtual void voice()
	{
		cout << "Animals: \n";
	}
	~Animal(){}
};
class Dog : public Animal
{
	virtual void voice()
	{
		cout << " I'm a dog \n";
	}
	virtual ~Dog(){}
};
class Cat : public Animal
{
	virtual void voice()
	{
		cout << " I'm a cat \n";
	}
	virtual ~Cat() {}
};
class Bear : public Animal
{
	virtual void voice()
	{
		cout << " I'm a bear \n";
	}
	virtual ~Bear() {}
};
int main()
{
	Animal* array[]{ new Animal, new Dog, new Cat, new Bear };
	for (int i = 0; i < 4; i++)
	{
		array[i]->voice();
		delete array[i];
	}
	
}

